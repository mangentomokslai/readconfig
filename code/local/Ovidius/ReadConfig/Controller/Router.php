<?php
class Ovidius_ReadConfig_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{

    public function initControllerRouters($observer)
    {

        // add router to config
        $front = $observer->getEvent()->getFront();
        $front->addRouter('readconfig', $this);

    }

    public function match(Zend_Controller_Request_Http $request)
    {
        $front = $this->getFront();
        $path = trim($request->getPathInfo(), '/');

        if ($path) {
            $p = explode('/', $path);
        } else
            return false;


        if (isset($p[0]))
            $module = $p[0];
        else
            return false;

        $controller = isset($p[1]) ? $p[1] : $front->getDefault('controller');
        $action = isset($p[2]) ? $p[2] : $front->getDefault('action');

        $frontName = Mage::getStoreConfig('readconfig_general_section/general/frontname');

        if ($p[0] == $frontName) {
            $request->setModuleName('readconfig')
                ->setControllerName($controller)
                ->setActionName($action);
            $request->setAlias(
                Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,
                $p
            );

            // set parameters from pathinfo
            for ($i = 3, $l = sizeof($p); $i < $l; $i += 2) {
                $request->setParam($p[$i], isset($p[$i+1]) ? urldecode($p[$i+1]) : '');
            }

            return true;
        } else
            return false;
    }
}
