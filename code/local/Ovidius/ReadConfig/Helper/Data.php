<?php
class Ovidius_ReadConfig_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function data()
    {
    }

    public function isLogEnabled()
    {
        return Mage::getStoreConfigFlag('readconfig_general_section/general/log');
    }

    public function getLogFile()
    {
        return Mage::getStoreConfig('readconfig_general_section/general/logfile');
    }

    public function log($message)
    {
        if ($this->isLogEnabled()) {
            $logFile = $this->getLogFile();
            Mage::log($message, null, $logFile);
        }
    }
}
