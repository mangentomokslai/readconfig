<?php
class Ovidius_ReadConfig_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $helper = Mage::helper('readconfig');

        $logEnabled = $helper->isLogEnabled() ? 'true' : 'false';
        $logFile = $helper->getlogFile();

        echo 'Reading configuration settings.' . '</br>';
        echo 'Logging is enabled: ' . $logEnabled . '</br>';
        echo 'Log file: ' . $logFile . '</br>';

        $helper->log('Accesed ' . $this->getRequest()->getPathInfo());

        if ($logEnabled == 'true')
            echo 'Logged a message to ' . $logFile;
    }

    public function dumplayoutAction()
    {
        $params = $this->getRequest()->getParams(); // parse parameters (key/value pairs as array) from url
        $outputAsXml = array_key_exists('xml', $params) ? true : false; // check if xml param is present


        /*
            Couple of notes:
                * <pre></pre> - tag used to encapsulate preformatted text.
                * htmlentities($string) - converts certain characters to html entities.
                  It's required in order for browser to render text verbatim instead of parsing it as HTML.

            Layout call sequence:
              $this->loadLayout() - initialization
              $this->getLayout() - calls Mage::getSingleton('core/layout') which gets layout object from registry
              $this->getUpdate() - gets the layout update object (Mage_Core_Layout_Update)
              $this->asString() - converts layout update array to string
        */
        $layoutDump = $this->loadLayout()->getLayout()->getUpdate()->asString();

        if($outputAsXml) {
            $this->getResponse()->setHeader('Content type', 'text/plain')->setBody($layoutDump);
        } else {
            echo "<pre>" . htmlentities($layoutDump) . "</pre>";
        }

    }
}
