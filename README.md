**This simple module demonstrates how to:**

* define helper class, 

* define route to controller action, 

* read config nodes

Once installed, module output is accessible via url: domain.com/readconfig

Update: Dynamic module route settable in module configuration page
Readmore: http://alanstorm.com/magento_dispatch_routers/